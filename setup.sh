#!/usr/bin/env bash
set -eo pipefail

requireBash4() {
  if ((BASH_VERSINFO < 4)); then
    >&2 echo "Bash 4.0 or newer is required for shopt."
    >&2 echo "If you're seeing this, you're probably on MacOS. Check the \"Probably Mac OS specific\" section in the README to upgrade."
    >&2 echo "Once you're upgraded, re-run this script."
    exit 1
  fi
}

get_relative_path () {
  if command -v realpath &> /dev/null
  then
    realpath --relative-to="${2-$PWD}" "$1"
  else
    python -c 'import os.path, sys;print os.path.relpath(sys.argv[1],sys.argv[2])' "$1" "${2-$PWD}"
  fi
}

# bash 4.0 or above is needed for shopt. Apple doesn't want to ship with >4.0 because it has GPL license :)
requireBash4

OS161_PATH="$1"
CS350_CONTAINER_PATH="$2"
VSCODE_WORKSPACE_PATH="$3"
export ASST_NUM="${4:-ASST2b}"

if ! [[ -d "$OS161_PATH" ]] || ! [[ -d "$CS350_CONTAINER_PATH" ]] || ! [[ -d "$VSCODE_WORKSPACE_PATH" ]]; then
    echo "This script sets up some files to enable vscode debugging"
    echo
    echo "Syntax: $0 <path_to_os161-1.99> <path_to_cs350-container> <path_to_vscode_workspace> [ASSTX]"
    echo
    if ! [[ -d "$OS161_PATH" ]]; then
        echo "Error: os161-1.99 path $OS161_PATH does not exist"
    fi
    if [[ ! -d "$CS350_CONTAINER_PATH" ]]; then
        echo "Error: cs350_container path $CS350_CONTAINER_PATH does not exist"
    fi
    if [[ ! -d "$VSCODE_WORKSPACE_PATH" ]]; then
        echo "Error: vscode workspace path $VSCODE_WORKSPACE_PATH does not exist"
    fi
    exit
fi

if [[ ! -f "$CS350_CONTAINER_PATH/os161-container/Dockerfile" ]]; then
    echo "Error: cs350_container dockerfile at $CS350_CONTAINER_PATH/Dockerfile does not exist. Have you cloned the repo?"
    exit
fi

if [[ ! -d "$CS350_CONTAINER_PATH/os161-container/assignments" ]]; then
    echo "Error: cs350_container assignments at $CS350_CONTAINER_PATH/assignments does not exist. Did you run install.sh yet?"
    exit
fi

if [[ ! -d "$CS350_CONTAINER_PATH/os161-container/dependencies" ]]; then
    echo "Error: your cs350_container doesn't have any dependencies at $CS350_CONTAINER_PATH/dependencies. Check that you have the right path and order of arguments?"
    exit
fi

if [[ ! -d "$OS161_PATH/kern" ]]; then
    echo "Error: No kern folder found at $OS161_PATH/kern. This is the code that is compiled in build_kernel. Check that you have the right path. "
    exit
fi

echo "Updating assignments repository"
git -C "$CS350_CONTAINER_PATH/os161-container/assignments" fetch
# idk how to do this better
git -C "$CS350_CONTAINER_PATH/os161-container/assignments" pull origin master

if [[ ! -d "$CS350_CONTAINER_PATH/os161-container/assignments/$ASST_NUM" ]]; then
    echo "Error: cs350_container assignments/$ASST_NUM at $CS350_CONTAINER_PATH/assignments does not exist. Make sure you have the correct assignment"
    exit
fi

KERNEL_CONF="$(sed -nr 's/build_kernel ([^ ]+).*/\1/p' "$CS350_CONTAINER_PATH"/os161-container/assignments/"$ASST_NUM"/run.sh)"


OS161_PATH_REL="./$(get_relative_path "$OS161_PATH" "$VSCODE_WORKSPACE_PATH")"
# echo $OS161_PATH_REL
CS350_CONTAINER_PATH_REL="./$(get_relative_path "$CS350_CONTAINER_PATH" "$VSCODE_WORKSPACE_PATH")"
# echo $CS350_CONTAINER_PATH_REL

# update gdb
if [[ ! -f "$CS350_CONTAINER_PATH/os161-container/dependencies/os161-gdb-old.tar.gz" ]]; then
    echo "updating os161 gdb"
    cp "$CS350_CONTAINER_PATH"/os161-container/dependencies/os161-gdb.tar.gz "$CS350_CONTAINER_PATH"/os161-container/dependencies/os161-gdb-old-predownload.tar.gz
    wget -O "$CS350_CONTAINER_PATH"/os161-container/dependencies/os161-gdb.tar.gz http://www.os161.org/download/gdb-7.8+os161-2.1.tar.gz
    mv "$CS350_CONTAINER_PATH"/os161-container/dependencies/os161-gdb-old-predownload.tar.gz "$CS350_CONTAINER_PATH"/os161-container/dependencies/os161-gdb-old.tar.gz
else
    echo "os161 gdb already updated, skipping"
fi

cp ./gdb_array_pretty_printer.py "$CS350_CONTAINER_PATH/os161-container/gdb_array_pretty_printer.py"

export OS161_PATH_REL CS350_CONTAINER_PATH_REL ASST_NUM BASIC_TESTS KERNEL_CONF
SUBSTITUTE='$OS161_PATH_REL:$CS350_CONTAINER_PATH_REL:$ASST_NUM:$KERNEL_CONF:$BASIC_TESTS'

echo "Discovering tests.."

shopt -s lastpipe
tests=()
find "$CS350_CONTAINER_PATH/os161-container/assignments/$ASST_NUM/basic" -type f -print0 |  while IFS= read -r -d '' test_file; do
    echo "$(basename "$test_file"): $(<"$test_file")"
    test_command="\\\"$(<"$test_file")\\\""
    tests+=("{\"label\":\"$(basename "$test_file")\",\"value\":\"$test_command\"}")
done
printf -v joined '%s, ' "${tests[@]}"
BASIC_TESTS="${joined%, }"
echo BASIC_TESTS
echo "Injecting docker-compose"
envsubst "$SUBSTITUTE" <docker-compose.yml >"$VSCODE_WORKSPACE_PATH"/docker-compose.yml

echo "Injecting vscode files"
mkdir -p "$VSCODE_WORKSPACE_PATH"/.vscode
envsubst "$SUBSTITUTE" <launch.json | python3 -m json.tool >"$VSCODE_WORKSPACE_PATH"/.vscode/launch.json

envsubst "$SUBSTITUTE" <tasks.json | python3 -m json.tool >"$VSCODE_WORKSPACE_PATH"/.vscode/tasks.json
echo "Building docker image"
docker-compose -f "$VSCODE_WORKSPACE_PATH"/docker-compose.yml build

echo "done"
