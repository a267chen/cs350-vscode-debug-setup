import gdb


class Sys161ArrayPrinter(object):
    "Print a sys161 array"

    def __init__(self, expr, is_ptr):
        self.arr_wrapper = expr
        self.arr_wrapper_type = self.arr_wrapper.type
        if self.arr_wrapper_type.code == gdb.TYPE_CODE_PTR:
            # print("is pointer")
            if self.arr_wrapper == 0:
                raise gdb.GdbError("null pointer exception")
            self.arr_wrapper = self.arr_wrapper.dereference()
            self.arr_wrapper_type = self.arr_wrapper.type
        # print(arr)
        # print(arr_type)
        # print(arr_type.tag)
        # print(arr_type.code)
        try:
            self.arr_item_type = gdb.parse_and_eval(
                f"{str(self.arr_wrapper_type.tag)}_get"
            ).type.target()
        except gdb.error as e:
            raise gdb.GdbError(f"{expr} is not an array")
        self.arr_length = self.arr_wrapper["arr"]["num"]

        self.arr = (
            self.arr_wrapper["arr"]["v"]
            .dereference()
            .cast(self.arr_item_type.array(0, int(self.arr_length)))
        )
        self.is_ptr = is_ptr

    def to_string(self):
        return f"{self.arr_wrapper_type}[{self.arr_length}]"

    def display_hint(self):
        return "array" if self.is_ptr else None

    def children(self):
        for i in range(int(self.arr_length)):
            yield (f"[{i}]", self.arr[i])


def arr_lookup_function(expr):
    # print("hi")
    arr_wrapper = expr
    arr_wrapper_type = expr.type
    is_ptr = False

    if arr_wrapper_type.code == gdb.TYPE_CODE_PTR:
        if expr == 0:
            return None
        arr_wrapper = arr_wrapper.dereference()
        arr_wrapper_type = arr_wrapper.type
        is_ptr = True
    lookup_tag = arr_wrapper_type.tag
    if lookup_tag is None:
        return None
    if str(lookup_tag) == "array":
        return None
    try:
        if (
            not arr_wrapper_type.fields()
            or not arr_wrapper_type.fields()[0].name == "arr"
        ):
            return None
    except TypeError:
        return None
    try:
        gdb.parse_and_eval(f"{str(arr_wrapper_type.tag)}_get").type.target()
        # gdb.write(f"{str(arr_wrapper_type)}\n")
    except gdb.error as e:
        return None
    # print("applies")
    return Sys161ArrayPrinter(arr_wrapper, is_ptr)


gdb.printing.register_pretty_printer(
    gdb.objfiles()[0], arr_lookup_function, replace=True
)
